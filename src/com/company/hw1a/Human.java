package com.company.hw1a;

public class Human {

    private  String lastName;
    private  String firstName;
    private  String middleName;

    public Human(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }
    public Human(String lastName, String firstName, String middleName) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
    }

    public String getFullName(){

        if(middleName == null) {
            return  String.join(" ",lastName,firstName);
        } else{
            return  String.join(" ",lastName,firstName,middleName);
        }

    }

    public String getShortName(){
        String shortName = lastName;

        if(firstName !=null && firstName.length()>0){
            shortName = shortName+" "+firstName.charAt(0)+".";
        }
        if (middleName !=null && middleName.length()>0) {
            shortName = shortName+" "+middleName.charAt(0)+".";
        }
        return  shortName;
    }
}