package com.company.hw1a;

public class Main {

    public static void main(String[] args) {
        Human human1 = new Human("Ivanov", "Ivan","Ivanovich");
        System.out.println(human1.getShortName());
        System.out.println(human1.getFullName());

        Human human2 = new Human("Petrov", "Petr");
        System.out.println(human2.getShortName());
        System.out.println(human2.getFullName());
    }
}
